import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Main from "./Containers/Main/Main";
import Hotels from "./Containers/Hotels/Hotels";
import Tours from "./Containers/Tours/Tours";
import Contacts from "./Containers/Contacts/Contacts";

const App = () => {

  return (
      <div className="App">
          <BrowserRouter>
            <Switch>
              <Route path="/" exact component={Main}/>
              <Route path="/hotels" component={Hotels}/>
              <Route path="/tours" component={Tours}/>
              <Route path="/contacts" component={Contacts}/>
              <Route render={() => <h1>Not found</h1>} />
            </Switch>
          </BrowserRouter>
      </div>
  )

}

export default App;
