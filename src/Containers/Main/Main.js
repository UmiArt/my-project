import React from 'react';
import './Main.css';

const Main = () => {
    return (
        <div className="Main">
            <div className="nav">
                <img className="logo" src="/images/logo.png"/>
                <nav>
                    <ul>
                        <li><a href="/hotels">Hotels</a></li>
                        <li><a href="/tours">Tours</a></li>
                        <li><a href="/contacts">Contacts</a></li>
                    </ul>
                </nav>

            </div>
            <div className="main-block">
                <h2>10 Powerful Reasons Why People Love to Travel</h2>
                <img className="main-pic" src="/images/Powerful.png"/>
            </div>

        </div>

    );
};

export default Main;