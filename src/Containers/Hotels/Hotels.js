import React from 'react';
import './Hotels.css';


const Hotels = ({history}) => {

    const back = () => {
        history.goBack();
    }

    const next = () => {
        history.replace('/tours/')
    }

    return (
        <div className="hotels">
            <div className="nav-hotel">
                <h5 onClick={back}>Back</h5>
                <h5 onClick={next}>Next</h5>
            </div>
            <div className="main-hotel">
                <h1>Dream Hotels</h1>
                <img className="hotel-pic" src="/images/hotels.png"/>
                <h1>The world’s best places to stay.</h1>
                <h4>Intimate retreats, sustainable safari camps,
                    gorgeous beach resorts … and so much more. There’s a world of dream stays to
                    discover within Virtuoso’s curated collection of more than 1,400 hotels, resorts, lodges, villas,
                    camps, and private islands. Ready to go? Your next getaway starts here.</h4>
                <img src="/images/all-hotels.png"/>
            </div>
            
        </div>
    );
};

export default Hotels;