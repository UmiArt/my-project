import React from 'react';

const Tours = ({history}) => {

    const back = () => {
        history.replace('/hotels/');
    }

    const next = () => {
        history.replace('/contacts/')
    }

    return (
        <div className="tours">
            <div className="nav-hotel">
                <h5 onClick={back}> Back </h5>
                <h5 onClick={next}> Next </h5>
            </div>
            <h1>Browse Tours</h1>
            <img className="sw-pic" src="/images/Hotel-Villa-Sw.png"/>
            <h4>Let’s Go: Virtuoso Tours & Experiences</h4>
            <img className="tour" src="/images/tour.png"/>

            
        </div>
    );
};

export default Tours;